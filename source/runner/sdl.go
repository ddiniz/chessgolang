//go:build sdl
// +build sdl

package runner

import (
	"chessgolang/source/sdl"
)

func init() {
	RunGame = sdl.RunGame
}

//go:build sdl
// +build sdl

package sdl

import (
	u "chessgolang/source/utils"
	"log"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

const textureSrc = "/textures/default/"

var files = []string{
	"tile1",
	"tile2",
	"b_pawn",
	"w_pawn",
	"b_king",
	"w_king",
	"b_queen",
	"w_queen",
	"b_bishop",
	"w_bishop",
	"b_horse",
	"w_horse",
	"b_tower",
	"w_tower",
	"valid_capture",
	"valid_move",
}

func loadImages() map[string]*sdl.Surface {
	folder := u.GetGameRootFolder(textureSrc)

	images := make(map[string]*sdl.Surface)
	for _, file := range files {
		image, err := img.Load(folder + file + ".png")
		if err != nil {
			log.Fatalf("Failed to load PNG: %s\n", err)
		}
		images[file] = image
	}
	return images
}

func unloadImages(images map[string]*sdl.Surface) {
	for _, value := range images {
		value.Free()
	}
}

func loadTextures(images map[string]*sdl.Surface, renderer *sdl.Renderer) map[string]*sdl.Texture {
	textures := make(map[string]*sdl.Texture)
	for key, value := range images {
		texture, err := renderer.CreateTextureFromSurface(value)
		if err != nil {
			log.Fatalf("Failed to create texture: %s\n", err)
		}
		textures[key] = texture
	}
	return textures
}

func unloadTextures(textures map[string]*sdl.Texture) {
	for _, value := range textures {
		value.Destroy()
	}
}

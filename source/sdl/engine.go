//go:build sdl
// +build sdl

package sdl

import (
	g "chessgolang/source/game"
	"log"
)

func GameLoop(game *g.Game) {
	switch game.State {
	case g.INITIALIZATION:
		log.Println("INITIALIZATION")
		game.ChangeState(g.INPUT)
	case g.INPUT:
		if Input.Save {
			log.Println("SAVED GAME")
			g.GameToFile(game, "savedstate")
		}
		if Input.Reset {
			game.ChangeState(g.RESTART)
		}
		if Input.Load {
			log.Println("LOADED GAME")
			*game = *g.FileToGame("savedstate")
			game.ChangeState(g.INPUT)
		}
		game.UpdatePiecePositions(Input.MousePos)
		if Input.MouseDown {
			recalculate := game.HandlePieceClicking(Input.MousePos)
			if recalculate {
				game.ChangeState(g.RECALCULATE_BOARD)
			}
		}
	case g.RECALCULATE_BOARD:
		log.Println("RECALCULATE_BOARD")
		ended := game.IsGameEnded()
		if ended {
			game.ChangeState(g.END)
			break
		}

		game.ChangeState(g.INPUT)

	case g.END:
		log.Println("END")
		game.ChangeState(g.RESTART)
	case g.RESTART:
		log.Println("RESTART")
		game.ChangeState(g.INITIALIZATION)
	}
}

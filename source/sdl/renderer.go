//go:build sdl
// +build sdl

package sdl

import (
	g "chessgolang/source/game"
	h "chessgolang/source/hitbox"
	p "chessgolang/source/piece"
	t "chessgolang/source/tile"

	"github.com/veandco/go-sdl2/sdl"
)

func RenderGame(sdlRenderer *sdl.Renderer, textures map[string]*sdl.Texture, game *g.Game) {
	renderBoard(sdlRenderer, game)
	renderValidMoves(sdlRenderer, game, textures)
	renderPieces(sdlRenderer, game)
	renderHand(sdlRenderer, game)
}

func hitboxToRect(hitbox h.Hitbox) *sdl.Rect {
	return &sdl.Rect{X: hitbox.X, Y: hitbox.Y, W: hitbox.W, H: hitbox.H}
}

func renderBoard(renderer *sdl.Renderer, game *g.Game) {
	for _, boardRow := range game.Board {
		for _, tile := range boardRow {
			renderer.Copy(
				tile.Texture,
				&sdl.Rect{X: 0, Y: 0, W: t.TILE_WIDTH, H: t.TILE_HEIGHT},
				tile.TexturePosition,
			)
		}
	}
}

func renderValidMoves(renderer *sdl.Renderer, game *g.Game, textures map[string]*sdl.Texture) {
	if game.Hand == nil {
		return
	}
	for _, pos := range game.Hand.ValidMoves {
		texture := textures["valid_move"]
		if game.Board[pos.X][pos.Y].Piece != nil {
			texture = textures["valid_capture"]
		}
		renderer.Copy(
			texture,
			&sdl.Rect{X: 0, Y: 0, W: t.TILE_WIDTH, H: t.TILE_HEIGHT},
			&sdl.Rect{
				X: pos.X * t.TILE_WIDTH, Y: pos.Y * t.TILE_HEIGHT,
				W: t.TILE_WIDTH, H: t.TILE_HEIGHT,
			},
		)
	}
}

func renderPieces(renderer *sdl.Renderer, game *g.Game) {
	for _, boardRow := range game.Board {
		for _, tile := range boardRow {
			if tile.Piece == nil {
				continue
			}
			piece := tile.Piece.(*p.Piece)
			renderer.Copy(
				piece.Texture,
				&sdl.Rect{X: 0, Y: 0, W: t.TILE_WIDTH, H: t.TILE_HEIGHT},
				piece.TexturePosition,
			)
		}
	}
}

func renderHand(renderer *sdl.Renderer, game *g.Game) {
	if game.Hand == nil {
		return
	}
	renderer.Copy(
		game.Hand.Texture,
		&sdl.Rect{X: 0, Y: 0, W: t.TILE_WIDTH, H: t.TILE_HEIGHT},
		&sdl.Rect{X: game.Hand.TexturePosition.X - 32, Y: game.Hand.TexturePosition.Y - 32, W: game.Hand.TexturePosition.W, H: game.Hand.TexturePosition.H},
	)
}

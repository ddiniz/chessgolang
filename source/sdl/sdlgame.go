//go:build sdl
// +build sdl

package sdl

import (
	g "chessgolang/source/game"
	"fmt"
	"os"

	"github.com/veandco/go-sdl2/sdl"
)

const (
	WINDOW_HEIGHT = int32(1060)
	WINDOW_WIDTH  = int32(768)
)

func RunGame() {
	window, sdlRenderer, _ := initializeSDL("Chess Golang", WINDOW_HEIGHT, WINDOW_WIDTH)
	defer sdl.Quit()
	defer window.Destroy()
	defer sdlRenderer.Destroy()
	images := loadImages()
	textures := loadTextures(images, sdlRenderer)
	defer unloadImages(images)
	defer unloadTextures(textures)

	sdl.JoystickEventState(sdl.ENABLE)
	game := g.NewGame(8, 8)
	game.InitializeGameChess(textures)

	for !Input.Quit {
		sdlRenderer.Clear()
		sdlRenderer.SetDrawColor(255, 255, 255, 255)
		sdlRenderer.FillRect(&sdl.Rect{X: 0, Y: 0, W: WINDOW_HEIGHT, H: WINDOW_WIDTH})

		GetInput()
		GameLoop(game)
		RenderGame(sdlRenderer, textures, game)

		sdlRenderer.Present()
		sdl.Delay(16)
	}
}

func initializeSDL(winTitle string, winWidth int32, winHeight int32) (*sdl.Window, *sdl.Renderer, error) {
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	window, err := sdl.CreateWindow(winTitle, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		winWidth, winHeight, sdl.WINDOW_SHOWN)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
		return nil, nil, err
	}

	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create renderer: %s\n", err)
		return nil, nil, err
	}
	return window, renderer, err
}

package tile

import (
	h "chessgolang/source/hitbox"
	u "chessgolang/source/utils"

	"github.com/veandco/go-sdl2/sdl"
)

const (
	TILE_WIDTH  = int32(64)
	TILE_HEIGHT = int32(64)
)

type Tile struct {
	Piece           any              `json:"piece"`
	Hitbox          h.Hitbox         `json:"hitbox"`
	Texture         *sdl.Texture     `json:"texture"`
	TexturePosition *sdl.Rect        `json:"texturePosition"`
	Position        u.Vector2[int32] `json:"position"`
}

func createHitboxAndTexturePosition(position u.Vector2[int32]) (h.Hitbox, *sdl.Rect) {
	x := int32(position.X) * TILE_WIDTH
	y := int32(position.Y) * TILE_HEIGHT
	hitbox := h.Hitbox{
		X: x,
		Y: y,
		W: TILE_WIDTH,
		H: TILE_HEIGHT,
	}
	texturePosition := &sdl.Rect{
		X: x,
		Y: y,
		W: TILE_WIDTH,
		H: TILE_HEIGHT,
	}
	return hitbox, texturePosition
}

func NewTile(piece any, textures map[string]*sdl.Texture, position u.Vector2[int32]) *Tile {
	hitbox, texturePosition := createHitboxAndTexturePosition(position)
	texture := textures["tile1"]
	if (position.X+position.Y)%2 == 0 {
		texture = textures["tile2"]
	}
	return &Tile{piece, hitbox, texture, texturePosition, position}
}

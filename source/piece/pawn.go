package piece

import (
	t "chessgolang/source/tile"
	u "chessgolang/source/utils"
	"log"
)

func GetPawnMovementOptions(board [][]*t.Tile, piece *Piece) []u.Vector2[int32] {
	//TODO: check en passant
	moves := make([]u.Vector2[int32], 0, 2)
	captures := make([]u.Vector2[int32], 0, 2)
	if piece == nil {
		log.Println("Trying to get movement options from a piece that doesn't exist.")
		return moves
	}

	directions := []u.Vector2[int32]{
		{X: 0, Y: +1},
	}
	if piece.FirstMove {
		directions = append(directions, u.Vector2[int32]{X: 0, Y: +2})
	}
	captureDirections := []u.Vector2[int32]{
		{X: -1, Y: +1},
		{X: +1, Y: +1},
	}

	if piece.Color == WHITE {
		negateDirectionsYPlane(directions)
		negateDirectionsYPlane(captureDirections)
	}

	getRawMovesAndCaptures(piece, directions, captureDirections, &moves, &captures)

	return getValidMovesAndCaptures(board, piece.Color, moves, captures)
}

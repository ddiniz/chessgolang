package piece

import (
	t "chessgolang/source/tile"
	u "chessgolang/source/utils"
)

type MovementFunction func(board [][]*t.Tile, piece *Piece) []u.Vector2[int32]

func isMoveInBounds(pos u.Vector2[int32], board [][]*t.Tile) bool {
	return u.CheckBound(pos.X, 0, int32(len(board))) && u.CheckBound(pos.Y, 0, int32(len(board[0])))
}

func getValidMovesAndCaptures(
	board [][]*t.Tile,
	pieceColor PieceColorEnum,
	moves []u.Vector2[int32],
	captures []u.Vector2[int32],
) []u.Vector2[int32] {
	return append(getValidMoves(board, moves), getValidCaptures(board, captures, pieceColor)...)
}

func getValidMoves(board [][]*t.Tile, positions []u.Vector2[int32]) []u.Vector2[int32] {
	validMoves := make([]u.Vector2[int32], 0)
	for _, position := range positions {
		if !isMoveInBounds(position, board) {
			continue
		}
		emptySpace := board[position.X][position.Y].Piece == nil
		if emptySpace {
			validMoves = append(validMoves, position)
		}
	}
	return validMoves
}

func getValidCaptures(board [][]*t.Tile, captures []u.Vector2[int32], pieceColor PieceColorEnum) []u.Vector2[int32] {
	validCaptures := make([]u.Vector2[int32], 0)
	for _, capture := range captures {
		if !isMoveInBounds(capture, board) {
			continue
		}
		emptySpace := board[capture.X][capture.Y].Piece == nil

		if !emptySpace {
			piece := board[capture.X][capture.Y].Piece.(*Piece)
			opposingTeam := piece.Color != pieceColor
			if opposingTeam {
				validCaptures = append(validCaptures, capture)
			}
		}
	}
	return validCaptures
}

func checkContinuousDirection(
	board [][]*t.Tile,
	piece *Piece,
	pos u.Vector2[int32],
	moves *[]u.Vector2[int32],
	captures *[]u.Vector2[int32],
) bool {
	if !isMoveInBounds(pos, board) {
		return true
	}
	if board[pos.X][pos.Y].Piece == nil {
		*moves = append(*moves, pos)
		return false
	} else {
		piece2 := board[pos.X][pos.Y].Piece.(*Piece)
		if piece2.Color != piece.Color {
			*captures = append(*captures, pos)
		}
		return true
	}
}

func getContinuousMovesAndCaptures(
	board [][]*t.Tile,
	piece *Piece,
	moves *[]u.Vector2[int32],
	captures *[]u.Vector2[int32],
	direction u.Vector2[int32],
) {
	for pos := (u.Vector2[int32]{X: piece.BoardIndex.X + direction.X, Y: piece.BoardIndex.Y + direction.Y}); true; pos.X, pos.Y = pos.X+direction.X, pos.Y+direction.Y {
		stop := checkContinuousDirection(board, piece, pos, moves, captures)
		if stop {
			break
		}
	}
}

func getRawMovesAndCaptures(piece *Piece, directions []u.Vector2[int32], captureDirections []u.Vector2[int32], moves *[]u.Vector2[int32], captures *[]u.Vector2[int32]) {
	for _, dir := range directions {
		*moves = append(*moves, u.Vector2[int32]{X: piece.BoardIndex.X + dir.X, Y: piece.BoardIndex.Y + dir.Y})
	}
	for _, dir := range captureDirections {
		*captures = append(*captures, u.Vector2[int32]{X: piece.BoardIndex.X + dir.X, Y: piece.BoardIndex.Y + dir.Y})
	}
}

func negateDirectionsYPlane(directions []u.Vector2[int32]) {
	for i := 0; i < len(directions); i++ {
		directions[i].Y = -directions[i].Y
	}
}

package piece

import (
	t "chessgolang/source/tile"
	u "chessgolang/source/utils"
	"log"
)

func GetTowerMovementOptions(board [][]*t.Tile, piece *Piece) []u.Vector2[int32] {
	moves := make([]u.Vector2[int32], 0, 16)
	captures := make([]u.Vector2[int32], 0, 4)

	if piece == nil {
		log.Println("Trying to get movement options from a piece that doesn't exist.")
		return moves
	}
	directions := []u.Vector2[int32]{
		{X: 0, Y: 1},
		{X: 0, Y: -1},
		{X: -1, Y: 0},
		{X: 1, Y: 0},
	}
	for _, dir := range directions {
		getContinuousMovesAndCaptures(board, piece, &moves, &captures, dir)
	}

	return append(moves, captures...)
}

package piece

import (
	t "chessgolang/source/tile"
	u "chessgolang/source/utils"
	"log"
)

func GetKingMovementOptions(board [][]*t.Tile, piece *Piece) []u.Vector2[int32] {
	//TODO: check checks
	moves := make([]u.Vector2[int32], 0, 8)

	if piece == nil {
		log.Println("Trying to get movement options from a piece that doesn't exist.")
		return moves
	}
	directions := []u.Vector2[int32]{
		{X: -1, Y: -1},
		{X: 0, Y: -1},
		{X: +1, Y: -1},
		{X: -1, Y: 0},
		{X: +1, Y: 0},
		{X: -1, Y: +1},
		{X: 0, Y: +1},
		{X: +1, Y: +1},
	}

	getRawMovesAndCaptures(piece, directions, nil, &moves, nil)

	return getValidMovesAndCaptures(board, piece.Color, moves, moves)
}

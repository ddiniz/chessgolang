package piece

import (
	h "chessgolang/source/hitbox"
	u "chessgolang/source/utils"
	"log"

	"github.com/veandco/go-sdl2/sdl"
)

const (
	PIECE_WIDTH  = int32(64)
	PIECE_HEIGHT = int32(64)
)

type Piece struct {
	Hitbox          h.Hitbox         `json:"hitbox"`
	Texture         *sdl.Texture     `json:"texture"`
	TexturePosition *sdl.Rect        `json:"texturePosition"`
	State           PieceStateEnum   `json:"state"`
	BoardIndex      u.Vector2[int32] `json:"boardIndex"`
	Type            PieceTypeEnum    `json:"type"`
	Color           PieceColorEnum   `json:"color"`
	Movement        MovementFunction
	ValidMoves      []u.Vector2[int32] `json:"validMoves"`
	FirstMove       bool               `json:"firstMove"`
	Revenge         bool               `json:"revenge"`
	Banana          bool               `json:"banana"`
	Jumped          bool               `json:"jumped"`
	PreviousMoves   []u.Vector2[int32] `json:"previousMoves"`
}

type PieceStateEnum uint8

const (
	BOARD PieceStateEnum = iota
	HAND
	DEAD
)

type PieceTypeEnum uint8

const (
	PAWN PieceTypeEnum = iota
	HORSE
	BISHOP
	TOWER
	QUEEN
	KING
	FISH
	FISHQUEEN
	CROW
	ELEPHANT
	MONKEY
	KING2
	QUEEN2
	BEAR
)

type PieceColorEnum uint8

const (
	WHITE PieceColorEnum = iota
	BLACK
	NEUTRAL
)

func createHitboxAndTexturePosition(position u.Vector2[int32]) (h.Hitbox, *sdl.Rect) {
	x := int32(position.X) * PIECE_WIDTH
	y := int32(position.Y) * PIECE_HEIGHT
	hitbox := h.Hitbox{
		X: x,
		Y: y,
		W: PIECE_WIDTH,
		H: PIECE_HEIGHT,
	}
	texturePosition := &sdl.Rect{
		X: x,
		Y: y,
		W: PIECE_WIDTH,
		H: PIECE_HEIGHT,
	}
	return hitbox, texturePosition
}

func NewPiece(
	textures map[string]*sdl.Texture,
	pieceType PieceTypeEnum,
	pieceColor PieceColorEnum,
	position u.Vector2[int32],
) *Piece {
	hitbox, texturePosition := createHitboxAndTexturePosition(position)
	piece := &Piece{
		Hitbox:          hitbox,
		TexturePosition: texturePosition,
		State:           BOARD,
		BoardIndex:      position,
		Type:            pieceType,
		Color:           pieceColor,
		ValidMoves:      make([]u.Vector2[int32], 0),
	}

	switch pieceType {
	case PAWN:
		piece.FirstMove = true
		piece.Movement = GetPawnMovementOptions
		if pieceColor == BLACK {
			piece.Texture = textures["b_pawn"]
		} else {
			piece.Texture = textures["w_pawn"]
		}
	case HORSE:
		piece.Movement = GetHorseMovementOptions
		if pieceColor == BLACK {
			piece.Texture = textures["b_horse"]
		} else {
			piece.Texture = textures["w_horse"]
		}
	case BISHOP:
		piece.Movement = GetBishopMovementOptions
		if pieceColor == BLACK {
			piece.Texture = textures["b_bishop"]
		} else {
			piece.Texture = textures["w_bishop"]
		}
	case TOWER:
		piece.Movement = GetTowerMovementOptions
		if pieceColor == BLACK {
			piece.Texture = textures["b_tower"]
		} else {
			piece.Texture = textures["w_tower"]
		}
	case QUEEN:
		piece.Movement = GetQueenMovementOptions
		if pieceColor == BLACK {
			piece.Texture = textures["b_queen"]
		} else {
			piece.Texture = textures["w_queen"]
		}
	case KING:
		piece.Movement = GetKingMovementOptions
		if pieceColor == BLACK {
			piece.Texture = textures["b_king"]
		} else {
			piece.Texture = textures["w_king"]
		}
	default:
		log.Fatal("Unknown piece type: ", pieceType)
	}

	return piece
}

func (piece *Piece) ChangeState(next PieceStateEnum) {
	switch piece.State {
	case BOARD:
		switch next {
		case BOARD:
			piece.State = next
		case HAND:
			piece.State = next
		case DEAD:
			piece.State = next
		}
	case HAND:
		switch next {
		case BOARD:
			piece.State = next
		case HAND:
			piece.State = next
		}
	case DEAD:
		switch next {
		case DEAD:
			piece.State = next
		}
	}
}

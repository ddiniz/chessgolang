package hitbox

import (
	u "chessgolang/source/utils"
)

type Hitbox struct {
	X int32
	Y int32
	W int32
	H int32
}

func (hitbox *Hitbox) InHitbox(pos u.Vector2[int32]) bool {
	return hitbox != nil && u.CheckBound(pos.X, hitbox.X, hitbox.W) && u.CheckBound(pos.Y, hitbox.Y, hitbox.H)
}

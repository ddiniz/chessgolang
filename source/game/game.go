package game

import (
	p "chessgolang/source/piece"
	t "chessgolang/source/tile"
	u "chessgolang/source/utils"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/veandco/go-sdl2/sdl"
)

type GameStateEnum uint8

const (
	INITIALIZATION GameStateEnum = iota
	INPUT
	RECALCULATE_BOARD
	END
	RESTART
)

type GameTypeEnum uint8

const (
	CHESS GameTypeEnum = iota
	CHESS2
)

type Game struct {
	Width  int           `json:"width"`
	Height int           `json:"height"`
	Board  [][]*t.Tile   `json:"board"`
	State  GameStateEnum `json:"state"`
	Hand   *p.Piece      `json:"hand"`
}

func NewGame(width, height int) *Game {
	return &Game{
		Width:  width,
		Height: height,
		Board:  newBoard(width, height),
		State:  INITIALIZATION,
		Hand:   nil,
	}
}

func newBoard(width, height int) [][]*t.Tile {
	board := make([][]*t.Tile, width)
	for i := 0; i < len(board); i++ {
		board[i] = make([]*t.Tile, height)
	}
	return board
}

func (game *Game) InitializeGameChess(textures map[string]*sdl.Texture) {
	for i := 0; i < len(game.Board); i++ {
		for j := 0; j < len(game.Board[i]); j++ {
			pos := u.Vector2[int32]{X: int32(i), Y: int32(j)}
			game.Board[i][j] = t.NewTile(nil, textures, pos)
		}
	}
	initializePlayerPieces(game, textures, p.BLACK, 1, 0)
	initializePlayerPieces(game, textures, p.WHITE, 6, 7)
}

func initializePlayerPieces(
	game *Game,
	textures map[string]*sdl.Texture,
	playerColor p.PieceColorEnum,
	pawnRow int,
	piecesRow int,
) {
	piecesFrontRow, piecesBackRow := getChessPiecePositions()

	for i := 0; i < len(piecesFrontRow); i++ {
		pos := u.Vector2[int32]{X: int32(i), Y: int32(pawnRow)}
		game.Board[i][pawnRow].Piece = p.NewPiece(textures, piecesFrontRow[i], playerColor, pos)
	}
	for i := 0; i < len(piecesBackRow); i++ {
		pos := u.Vector2[int32]{X: int32(i), Y: int32(piecesRow)}
		game.Board[i][piecesRow].Piece = p.NewPiece(textures, piecesBackRow[i], playerColor, pos)
	}
}

func getChessPiecePositions() ([]p.PieceTypeEnum, []p.PieceTypeEnum) {
	piecesBackRow := []p.PieceTypeEnum{
		p.TOWER,
		p.HORSE,
		p.BISHOP,
		p.QUEEN,
		p.KING,
		p.BISHOP,
		p.HORSE,
		p.TOWER,
	}
	piecesFrontRow := []p.PieceTypeEnum{
		p.PAWN,
		p.PAWN,
		p.PAWN,
		p.PAWN,
		p.PAWN,
		p.PAWN,
		p.PAWN,
		p.PAWN,
	}
	return piecesFrontRow, piecesBackRow
}

func (game *Game) UpdatePiecePositions(mousePos u.Vector2[int32]) {
	if game.Hand != nil {
		game.Hand.TexturePosition.X = mousePos.X
		game.Hand.TexturePosition.Y = mousePos.Y
	}
	for i, boardRow := range game.Board {
		for j, tile := range boardRow {
			pieceAny := tile.Piece
			if pieceAny == nil {
				continue
			}
			piece := tile.Piece.(*p.Piece)
			x := int32(i) * t.TILE_WIDTH
			y := int32(j) * t.TILE_HEIGHT
			piece.Hitbox.X = x
			piece.TexturePosition.X = x
			piece.Hitbox.Y = y
			piece.TexturePosition.Y = y
		}
	}
}

func (game *Game) HandlePieceClicking(mousePos u.Vector2[int32]) bool {
	if game.Hand == nil {
		return handlePieceClickingWithEmptyHand(game, mousePos)
	}
	return handleClickingWithPieceInHand(game, mousePos)
}

func handlePieceClickingWithEmptyHand(game *Game, mousePos u.Vector2[int32]) bool {
	clickedPiece := getClickedPiece(game.Board, mousePos)
	if clickedPiece == nil || game.Hand != nil {
		return false
	}

	game.Hand = clickedPiece
	game.Hand.ValidMoves = game.Hand.Movement(game.Board, game.Hand)
	game.Board[clickedPiece.BoardIndex.X][clickedPiece.BoardIndex.Y].Piece = nil
	clickedPiece.ChangeState(p.HAND)
	return true
}

func handleClickingWithPieceInHand(game *Game, mousePos u.Vector2[int32]) bool {
	clickedTile := getClickedTile(game.Board, mousePos)
	if clickedTile == nil {
		return false
	}
	if clickedTile.Position.X == game.Hand.BoardIndex.X && clickedTile.Position.Y == game.Hand.BoardIndex.Y {
		movePieceToBoard(game, clickedTile)
		return true
	}
	for _, move := range game.Hand.ValidMoves {
		if move.X == clickedTile.Position.X && move.Y == clickedTile.Position.Y {
			if game.Hand.FirstMove {
				game.Hand.FirstMove = false
			}
			movePieceToBoard(game, clickedTile)
			return true
		}
	}
	return false
}

func movePieceToBoard(game *Game, clickedTile *t.Tile) {
	game.Hand.ChangeState(p.BOARD)
	game.Hand.BoardIndex.X = clickedTile.Position.X
	game.Hand.BoardIndex.Y = clickedTile.Position.Y
	game.Board[clickedTile.Position.X][clickedTile.Position.Y].Piece = game.Hand
	game.Hand = nil
}

func getClickedPiece(board [][]*t.Tile, mousePos u.Vector2[int32]) *p.Piece {
	for _, boardRow := range board {
		for _, tile := range boardRow {
			pieceAny := tile.Piece
			if pieceAny == nil {
				continue
			}
			piece := tile.Piece.(*p.Piece)
			if piece.Hitbox.InHitbox(mousePos) {
				return piece
			}
		}
	}
	return nil
}

func getClickedTile(board [][]*t.Tile, mousePos u.Vector2[int32]) *t.Tile {
	for _, boardRow := range board {
		for _, tile := range boardRow {
			if tile != nil && tile.Hitbox.InHitbox(mousePos) {
				return tile
			}
		}
	}
	return nil
}

func (game *Game) IsGameEnded() bool {
	return false
}

func printBadState(game *Game, next GameStateEnum) {
	log.Println("Bad game state, current:", game.State, " next:", next)
}

func (game *Game) ChangeState(next GameStateEnum) {
	switch game.State {
	case INITIALIZATION:
		switch next {
		case INPUT:
			game.State = INPUT
		default:
			printBadState(game, next)
		}
	case INPUT:
		switch next {
		case RECALCULATE_BOARD:
			game.State = RECALCULATE_BOARD
		case RESTART:
			game.State = RESTART
		default:
			printBadState(game, next)
		}
	case RECALCULATE_BOARD:
		switch next {
		case INPUT:
			game.State = INPUT
		case END:
			game.State = END
		default:
			printBadState(game, next)
		}
	case END:
		switch next {
		case END:
			game.State = END
		default:
			printBadState(game, next)
		}
	case RESTART:
		switch next {
		case INITIALIZATION:
			game.State = INITIALIZATION
		default:
			printBadState(game, next)
		}
	}
}

func GameToFile(game *Game, filename string) {
	folder := u.GetGameRootFolder("/")
	readFile, err := os.Create(folder + filename + ".json")
	if err != nil {
		fmt.Println(err)
	}
	json, err := json.Marshal(game)
	if err != nil {
		fmt.Println(err)
	}
	readFile.WriteString(string(json))
	readFile.Close()
}

func FileToGame(filename string) *Game {
	folder := u.GetGameRootFolder("/")
	dat, err := os.ReadFile(folder + filename)
	if err != nil {
		fmt.Println(err)
	}
	game := &Game{}
	json.Unmarshal(dat, game)
	return game
}

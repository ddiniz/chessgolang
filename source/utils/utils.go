package utils

import (
	"log"
	"os"
	"path/filepath"
)

func GetLast[T any](array []*T) *T {
	if len(array) > 0 {
		return array[len(array)-1]
	}
	return nil
}

func IsEmpty[T any](array []*T) bool {
	return len(array) == 0
}

func CheckBound(val, x, w int32) bool {
	return x <= val && val < x+w
}

func GetGameRootFolder(path string) string {
	folder := GetExecutablePath() + path //This to find if debug used
	if _, err := os.Stat(folder); !os.IsNotExist(err) {
		return folder
	}

	folder = GetCurrentDir() + path //This to find if go run used
	if _, err := os.Stat(folder); !os.IsNotExist(err) {
		return folder
	}

	log.Fatal("Unable to locate texture folder")
	return ""
}

func GetCurrentDir() string {
	path, err := os.Getwd()
	if err != nil {
		log.Fatalf("Unable to open current dir with err: %s", err)
	}
	return path
}

func GetExecutablePath() string {
	ex, err := os.Executable()
	if err != nil {
		log.Fatalf("Unable to locate executable current dir with err: %s", err)
	}
	return filepath.Dir(ex)
}

package utils

type Vector2[T any] struct {
	X T
	Y T
}

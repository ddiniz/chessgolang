package main

import (
	"chessgolang/source/runner"
	"math/rand"
	"time"
)

// TODO: check if king is in check
// TODO: check if game ended
// TODO: restart game correctly
// TODO: implement turns
// TODO: online multiplayer
// TODO: label board
// TODO: center board on screen or reduce screen size
// TODO: show dangerous spaces (spaces where you piece can be captured)?
// TODO: game graphics
// TODO: implement castling
// TODO: implement en passant
// TODO: implement piece promotion
// TODO: I'm going to have to implement a GUI, won't I?

func main() {
	rand.Seed(time.Now().UnixNano())
	runner.RunGame()
}

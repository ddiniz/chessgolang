module chessgolang

go 1.18

require (
	github.com/stretchr/testify v1.8.1
	github.com/ttacon/chalk v0.0.0-20160626202418-22c06c80ed31
	github.com/veandco/go-sdl2 v0.4.28
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
